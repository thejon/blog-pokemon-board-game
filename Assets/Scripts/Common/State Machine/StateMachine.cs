﻿[System.Serializable]
public class StateMachine {
	public State current;

	public void ChangeState (State target)
	{
		if (current == target)
			return;

		if (current != null)
			current.Exit();

		current = target;

		if (current != null)
			current.Enter();
	}
}