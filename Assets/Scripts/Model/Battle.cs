﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;
using System;

public class Battle {
	public List<Combatant> combatants = new List<Combatant>();
	public Move move;
	public int lastDamage;
	public Combatant attacker { get { return combatants [0]; } }
	public Combatant defender { get { return combatants [1]; } }
}
