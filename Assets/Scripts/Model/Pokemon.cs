﻿using UnityEngine;
using System;
using ECS;

[Serializable]
public class Pokemon {

	public static readonly double[] multiplierPerLevel = new double[] { 0.094, 0.135137432, 0.16639787, 0.192650919, 0.21573247, 
		0.236572661, 0.25572005, 0.273530381, 0.29024988, 0.306057377, 0.3210876, 0.335445036, 0.34921268, 0.362457751, 
		0.37523559, 0.387592406, 0.39956728, 0.411193551, 0.42250001, 0.432926419, 0.44310755, 0.453059958, 0.46279839, 
		0.472336083, 0.48168495, 0.4908558, 0.49985844, 0.508701765, 0.51739395, 0.525942511, 0.53435433, 0.542635767, 
		0.55079269, 0.558830576, 0.56675452, 0.574569153, 0.58227891, 0.589887917, 0.59740001, 0.604818814, 0.61215729, 
		0.619399365, 0.62656713, 0.633644533, 0.64065295, 0.647576426, 0.65443563, 0.661214806, 0.667934, 0.674577537, 
		0.68116492, 0.687680648, 0.69414365, 0.700538673, 0.70688421, 0.713164996, 0.71939909, 0.725571552, 0.7317, 
		0.734741009, 0.73776948, 0.740785574, 0.74378943, 0.746781211, 0.74976104, 0.752729087, 0.75568551, 0.758630378, 
		0.76156384, 0.764486065, 0.76739717, 0.770297266, 0.7731865, 0.776064962, 0.77893275, 0.781790055, 0.78463697, 
		0.787473578, 0.79030001 
	};

	public static int MaxLevel { get { return multiplierPerLevel.Length - 1; }}
	public const int MaxEnergy = 100;

	public Genders gender;
	public int entityID;
	public int fastMoveID;
	public int chargeMoveID;
	public int hitPoints;
	public int maxHitPoints;
	public int energy;
	public int level;
	public int attackIV;
	public int defenseIV;
	public int staminaIV;

	public string Name { get; set; }
	public Entity Entity { get; set; }
	public Move FastMove { get; set; }
	public Move ChargeMove { get; set; }
	public SpeciesStats Stats { get; set; }
	public Evolvable Evolvable { get; set; }

	public float HPRatio { get { return (float)hitPoints / (float)maxHitPoints; }}
	public float EnergyRatio { get { return (float)energy / (float)MaxEnergy; }}
	public float LevelRatio { get { return (float)level / (float)MaxLevel; }}

	public float CPM { get { return (float)multiplierPerLevel[level]; }}
	public float Attack { get { return (Stats.attack + attackIV) * CPM; } }
	public float Defense { get { return (Stats.defense + defenseIV) * CPM; }}
	public float Stamina { get { return (Stats.stamina + staminaIV) * CPM; }}
	public int CP { get { return Mathf.Max(10, Mathf.FloorToInt(Mathf.Sqrt(Attack * Attack * Defense * Stamina) / 10.0f)); }}
}