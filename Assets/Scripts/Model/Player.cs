﻿using System.Collections.Generic;
using System;

[Serializable]
public class Player {
	public string nickName;
	public int tileIndex;
	public int destinationTileIndex;
	public int pokeballs;
	public int revives;
	public int potions;
	public int candies;
	public List<Pokemon> pokemon = new List<Pokemon>();
	public List<string> badges = new List<string>();
}