﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Billboard : MonoBehaviour {

	public Transform target;
	Transform cachedTransform;

	void OnEnable () {
		if (cachedTransform == null)
			cachedTransform = transform;
		if (target == null)
			target = Camera.main.transform;
	}

	void Update () {
		cachedTransform.LookAt (target);
	}
}
