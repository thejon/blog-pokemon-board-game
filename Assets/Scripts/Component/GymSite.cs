﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GymSite : MonoBehaviour {
	public int index;
	[SerializeField] Transform display;

	public void Refresh (Gym gym) {
		SpriteRenderer sr = display.GetComponentInChildren<SpriteRenderer>();
		sr.sprite = gym.GetBadge();
	}
}