﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Board : MonoBehaviour {
	public GameObject tilePrefab;
	public List<Transform> tiles = new List<Transform>();
}