﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TileMarker : MonoBehaviour {

	public Transform display;

	void Start () {
		Tweener tweener = display.transform.RotateToLocal(new Vector3(0, 360, 0), 3, EasingEquations.Linear);
		tweener.loopCount = -1;
	}
}
