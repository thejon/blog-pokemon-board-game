﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class AttackCandidateView : MonoBehaviour {

	public Action didPressLeft;
	public Action didPressRight;

	public Text nameLabel;
	public Text cpLabel;
	public Text hpLabel;
	public Image avatarImage;
	public Button previousButton;
	public Button nextButton;

	public void Display (Pokemon pokemon) {
		nameLabel.text = pokemon.Name;
		cpLabel.text = string.Format ("CP:{0}", pokemon.CP);
		hpLabel.text = string.Format ("HP: {0}/{1}", pokemon.hitPoints, pokemon.maxHitPoints);
		avatarImage.sprite = pokemon.GetAvatar ();
	}
}