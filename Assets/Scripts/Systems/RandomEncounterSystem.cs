﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public static class RandomEncounterSystem {
	const float spawnRate = 0.2f;

	static List<Encounterable> Table {
		get {
			if (_table == null) {
				var all = DataController.instance.pokemonDatabase.connection.Table<Encounterable>();
				_table = new List<Encounterable>(all);
				foreach (Encounterable item in all)
					wheel += item.rate;
			}
			return _table;
		}
	}
	static List<Encounterable> _table;
	static double wheel;

	public static Pokemon Apply (Player player, Board board) {
		var tileIndex = player.tileIndex;
		var tile = board.tiles[tileIndex];
		SpawnPokemon component = tile.GetComponent<SpawnPokemon>();

		if (component == null || Random.value > spawnRate)
			return null;

		Encounterable data = Table.First();
		double randomValue = Random.value * wheel;
		double sum = 0;
		foreach (Encounterable item in Table) {
			sum += item.rate;
			if (sum >= randomValue) {
				data = item;
				break;
			}
		}

		Entity entity = data.GetEntity();
		var level = UnityEngine.Random.Range(0, player.pokemon[0].level);
		var pokemon = PokemonFactory.Create(entity, level);
		return pokemon;
	}
}