﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class HealSystem {
	public static void FullHeal (Pokemon pokemon) {
		Heal(pokemon, pokemon.maxHitPoints);
	}

	public static void Heal (Pokemon pokemon, int amount) {
		pokemon.hitPoints = Mathf.Min(pokemon.maxHitPoints, pokemon.hitPoints + amount);
	}
}
