﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PotionSystem {

	public static bool CanApply (Player player, Pokemon pokemon) {
		return player.potions > 0 && pokemon.hitPoints > 0 && pokemon.hitPoints < pokemon.maxHitPoints;
	}

	public static void Apply (Player player, Pokemon pokemon) {
		HealSystem.Heal(pokemon, 20);
		player.potions--;
	}
}
