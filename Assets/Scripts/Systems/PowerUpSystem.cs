﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PowerUpSystem {

	public static bool CanApply (Player player, Pokemon pokemon) {
		int nextLevel = pokemon.level + 1;
		return (nextLevel <= Pokemon.MaxLevel) && player.candies >= nextLevel;
	}

	public static int GetCost (Pokemon pokemon) {
		return pokemon.level + 1;
	}

	public static void Apply (Player player, Pokemon pokemon) {
		int oldMax = pokemon.maxHitPoints;
		pokemon.SetLevel(pokemon.level + 1);
		int delta = pokemon.maxHitPoints - oldMax;
		HealSystem.Heal (pokemon, delta);
		player.candies -= pokemon.level;
	}
}