﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State GameOverState {
		get {
			if (_endState == null)
				_endState = new State(OnEnterGameOverState, null, "GameOver");
			return _endState;
		}
	}
	State _endState;

	void OnEnterGameOverState () {
		DataController.instance.ClearSavedGame ();
		musicController.PlayGameOver();
		var title = "Game Over";
		var message = string.Format ("{0} wins!", game.CurrentPlayer.nickName);
		dialogViewController.Show (title, message, delegate {
			dialogViewController.didComplete = delegate {
				dialogViewController.didComplete = null;
				dialogViewController.Hide(delegate {
					stateMachine.ChangeState(IntroState);
				});
			};
		});
	}
}
