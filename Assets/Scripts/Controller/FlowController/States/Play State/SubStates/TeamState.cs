﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State TeamState {
		get {
			if (_teamState == null)
				_teamState = new State(OnEnterTeamState, null, "Team");
			return _teamState;
		}
	}
	State _teamState;

	void OnEnterTeamState () {
		teamViewController.Show (delegate {
			teamViewController.didComplete = delegate {
				teamViewController.didComplete = null;
				teamViewController.Hide(delegate {
					stateMachine.ChangeState(GetSetState);
				});
			};
		});
	}
}
