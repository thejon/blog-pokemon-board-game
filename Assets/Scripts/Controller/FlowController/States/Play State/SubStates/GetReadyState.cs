﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {
	State GetReadyState {
		get { 
			if (_getReadyState == null)
				_getReadyState = new State(OnEnterGetReadyState, null, "Get Ready");
			return _getReadyState;
		}
	}
	State _getReadyState;

	void OnEnterGetReadyState () {
		DataController.instance.SaveGame ();
		musicController.PlayJourney (game.CurrentPlayer.badges.Count);
		var title = string.Format ("{0}'s Turn!", game.CurrentPlayer.nickName);
		dialogViewController.Show (title, null, delegate {
			dialogViewController.didComplete = delegate {
				dialogViewController.didComplete = null;
				dialogViewController.Hide (delegate {
					stateMachine.ChangeState(GetSetState);
				});
			};
		});
	}
}
