﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public partial class FlowController : MonoBehaviour {

	State GymCommandState {
		get {
			if (_gymCommandState == null)
				_gymCommandState = new State(OnEnterGymCommandState, null, "GymCommand");
			return _gymCommandState;
		}
	}
	State _gymCommandState;

	void OnEnterGymCommandState () {
		CombatSystem.Next(battle);
		if (battle.attacker.mode == ControlModes.Player) {
			commandViewController.Show(delegate {
				commandViewController.didFinish = delegate(CommandViewController.Exits obj) {
					commandViewController.didFinish = null;
					commandViewController.Hide(delegate {
						switch (obj) {
						case CommandViewController.Exits.FastMove:
							SelectGymMove (battle.attacker.CurrentPokemon.FastMove);
							break;
						case CommandViewController.Exits.ChargeMove:
							SelectGymMove (battle.attacker.CurrentPokemon.ChargeMove);
							break;
						case CommandViewController.Exits.Retreat:
							stateMachine.ChangeState (DefeatState);
							break;
						}
					});
				};
			});
		} else {
			SelectGymMove (CombatSystem.PickMove (battle));
		}
	}

	void SelectGymMove (Move move) {
		battle.move = move;
		stateMachine.ChangeState (GymAttackState);
	}
}
