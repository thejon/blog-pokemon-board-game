﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State TagTeamState {
		get {
			if (_tagTeamState == null)
				_tagTeamState = new State(OnEnterTagTeamState, null, "TagTeam");
			return _tagTeamState;
		}
	}
	State _tagTeamState;

	void OnEnterTagTeamState () {
		if (CombatSystem.SwapIfNeeded(battle)) {
			combatViewController.Display();
		}
		if (battle.defender.CurrentPokemon.hitPoints == 0) {
			if (battle.attacker.mode == ControlModes.Player)
				stateMachine.ChangeState (VictoryState);
			else
				stateMachine.ChangeState (DefeatState);
		} else {
			stateMachine.ChangeState (GymCommandState);
		}
	}
}
