﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State CheckDestinationState {
		get {
			if (_checkDestinationState == null)
				_checkDestinationState = new State(OnEnterCheckDestinationState, null, "CheckDestination");
			return _checkDestinationState;
		}
	}
	State _checkDestinationState;

	void OnEnterCheckDestinationState () {
		Player current = game.CurrentPlayer;
		if (GameSystem.IsGameOver(game) || current.tileIndex == current.destinationTileIndex) {
			stateMachine.ChangeState (CheckWinState);
		} else {
			stateMachine.ChangeState (MoveState);
		}
	}
}
