﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State DefeatState {
		get {
			if (_defeatState == null)
				_defeatState = new State(OnEnterDefeatState, null, "Defeat");
			return _defeatState;
		}
	}
	State _defeatState;

	void OnEnterDefeatState () {
		game.CurrentPlayer.destinationTileIndex = game.CurrentPlayer.tileIndex;
		defeatrViewController.Show (delegate {
			defeatrViewController.didFinish = delegate {
				defeatrViewController.didFinish = null;
				defeatrViewController.Hide(delegate {
					stateMachine.ChangeState (GymCompleteState);
				});
			};
		});
	}
}
