﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State AttackTeamState {
		get {
			if (_attackTeamState == null)
				_attackTeamState = new State(OnEnterAttackTeamState, null, "AttackTeam");
			return _attackTeamState;
		}
	}
	State _attackTeamState;

	void OnEnterAttackTeamState () {
		attackTeamViewController.Show (delegate {
			attackTeamViewController.didFinish = delegate(Battle obj) {
				attackTeamViewController.didFinish = null;
				attackTeamViewController.Hide(delegate {
					if (obj != null) {
						battle = obj;
						musicController.PlayBattle(1);
						commandViewController.SetupGymBattle ();
						combatViewController.gameObject.SetActive(true);
						stateMachine.ChangeState (GymCommandState);
					} else {
						stateMachine.ChangeState (GymCompleteState);
					}
				});
			};
		});
	}
}
