﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State JourneyState {
		get {
			if (_journeyState == null)
				_journeyState = new State(OnEnterJourneyState, null, "Journey");
			return _journeyState;
		}
	}
	State _journeyState;

	void OnEnterJourneyState () {
		stateMachine.ChangeState (MoveState);
	}
}
