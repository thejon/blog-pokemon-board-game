﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State PokeCenterState {
		get {
			if (_pokeCenterState == null)
				_pokeCenterState = new State(OnEnterPokeCenterState, null, "Poke Center");
			return _pokeCenterState;
		}
	}
	State _pokeCenterState;

	void OnEnterPokeCenterState () {
		if (PokeCenterSystem.HealPokemon(game, board)) {
			dialogViewController.Show ("Poke Center", "Your Pokemon have all been restored!", delegate {
				dialogViewController.didComplete = delegate {
					dialogViewController.didComplete = null;
					dialogViewController.Hide(delegate {
						stateMachine.ChangeState (PokestopState);
					});
				};
			});
		} else {
			stateMachine.ChangeState (PokestopState);
		}
	}
}