﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State GymState {
		get {
			if (_gymState == null)
				_gymState = new State(OnEnterGymState, null, "Gym");
			return _gymState;
		}
	}
	State _gymState;

	void OnEnterGymState () {
		var gym = GymSystem.GetCurrentGym(game, board);
		if (gym != null && GymSystem.CanChallenge(game.CurrentPlayer, gym)) {
			gymEncounterViewController.Show (gym, delegate {
				gymEncounterViewController.didFinish = delegate(GymEncounterViewController.Exits obj) {
					gymEncounterViewController.didFinish = null;
					gymEncounterViewController.Hide(delegate {
						switch(obj) {
						case GymEncounterViewController.Exits.Challenge:
							stateMachine.ChangeState (AttackTeamState);
							break;
						case GymEncounterViewController.Exits.Ignore:
							stateMachine.ChangeState (CheckDestinationState);
							break;
						}
					});
				};
			});
		} else {
			stateMachine.ChangeState (CheckDestinationState);
		}
	}
}