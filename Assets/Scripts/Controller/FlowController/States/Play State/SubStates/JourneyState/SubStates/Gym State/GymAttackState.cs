﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State GymAttackState {
		get {
			if (_gymAttackState == null)
				_gymAttackState = new State(OnEnterGymAttackState, null, "GymAttack");
			return _gymAttackState;
		}
	}
	State _gymAttackState;

	void OnEnterGymAttackState () {
		CombatSystem.ApplyMove(battle);
		combatViewController.didCompleteMove = delegate {
			if (battle.defender.CurrentPokemon.hitPoints == 0) {
				stateMachine.ChangeState (TagTeamState);
			} else {
				stateMachine.ChangeState (GymCommandState);
			}
		};
		combatViewController.ApplyMove();
	}
}
