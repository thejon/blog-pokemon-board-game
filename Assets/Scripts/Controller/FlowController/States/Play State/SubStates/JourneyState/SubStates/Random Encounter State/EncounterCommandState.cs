﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public partial class FlowController : MonoBehaviour {

	State EncounterCommandState {
		get {
			if (_encounterCommandState == null)
				_encounterCommandState = new State(OnEnterEncounterCommandState, null, "EncounterCommand");
			return _encounterCommandState;
		}
	}
	State _encounterCommandState;

	void OnEnterEncounterCommandState () {
		CombatSystem.Next(battle);
		if (battle.attacker.mode == ControlModes.Player) {
			commandViewController.Show(delegate {
				commandViewController.didFinish = delegate(CommandViewController.Exits obj) {
					commandViewController.didFinish = null;
					commandViewController.Hide(delegate {
						switch (obj) {
						case CommandViewController.Exits.FastMove:
							SelectEncounterMove (battle.attacker.CurrentPokemon.FastMove);
							break;
						case CommandViewController.Exits.ChargeMove:
							SelectEncounterMove (battle.attacker.CurrentPokemon.ChargeMove);
							break;
						case CommandViewController.Exits.Capture:
							stateMachine.ChangeState (CaptureState);
							break;
						}
					});
				};
			});
		} else {
			SelectEncounterMove (CombatSystem.PickMove (battle));
		}
	}

	void SelectEncounterMove (Move move) {
		battle.move = move;
		stateMachine.ChangeState (EncounterAttackState);
	}
}
