﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State MoveState {
		get {
			if (_moveState == null)
				_moveState = new State(OnEnterMoveState, OnExitMoveState, "Move");
			return _moveState;
		}
	}
	State _moveState;

	void OnEnterMoveState () {
		gameViewController.moveComplete = delegate {
			stateMachine.ChangeState (PokeCenterState);
		};
		gameViewController.MovePawn();
	}

	void OnExitMoveState () {
		gameViewController.moveComplete = null;
	}
}
