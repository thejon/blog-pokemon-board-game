﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State RollState {
		get {
			if (_rollState == null)
				_rollState = new State(OnEnterRollState, null, "Roll");
			return _rollState;
		}
	}
	State _rollState;

	void OnEnterRollState () {
		rollViewController.Show (delegate {
			rollViewController.didComplete = delegate {
				rollViewController.didComplete = null;
				rollViewController.Hide(delegate {
					stateMachine.ChangeState (JourneyState);
				});
			};
		});
	}
}
