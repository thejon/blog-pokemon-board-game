﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State SetupCompleteState {
		get {
			if (_setupCompleteState == null)
				_setupCompleteState = new State(OnEnterSetupCompleteState, null, "SetupComplete");
			return _setupCompleteState;
		}
	}
	State _setupCompleteState;

	void OnEnterSetupCompleteState () {
		gameViewController.LoadGame();
		stateMachine.ChangeState (PlayState);
	}
}