﻿using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State MenuState {
		get {
			if (_menuState == null)
				_menuState = new State(OnEnterMenuState, OnExitMenuState, "Menu");
			return _menuState;
		}
	}
	State _menuState;

	void OnEnterMenuState () {
		introMenuViewController.gameObject.SetActive (true);
		introMenuViewController.didFinish = delegate(IntroMenuViewController.Exits obj) {
			switch (obj) {
			case IntroMenuViewController.Exits.New:
				stateMachine.ChangeState (CreateState);
				break;
			case IntroMenuViewController.Exits.Load:
				stateMachine.ChangeState (LoadState);
				break;
			}
		};
	}

	void OnExitMenuState () {
		introMenuViewController.didFinish = null;
		introMenuViewController.gameObject.SetActive (false);
		introLogoViewController.gameObject.SetActive (false);
	}
}