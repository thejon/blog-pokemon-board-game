﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State CreateState {
		get {
			if (_createState == null)
				_createState = new State(OnEnterCreateState, null, "Create");
			return _createState;
		}
	}
	State _createState;

	void OnEnterCreateState () {
		stateMachine.ChangeState (PlayerCountState);
	}
}