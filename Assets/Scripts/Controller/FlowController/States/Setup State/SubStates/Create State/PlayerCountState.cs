﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public partial class FlowController : MonoBehaviour {

	State PlayerCountState {
		get {
			if (_playerCountState == null)
				_playerCountState = new State(OnEnterPlayerCountState, OnExitPlayerCountState, "Player Count");
			return _playerCountState;
		}
	}
	State _playerCountState;

	void OnEnterPlayerCountState () {
		playerCountViewController.gameObject.SetActive(true);
		playerCountViewController.didComplete = delegate(int count) {
			game = GameFactory.Create(count);
			stateMachine.ChangeState(PlayerConfigureState);
		};
	}

	void OnExitPlayerCountState () {
		playerCountViewController.didComplete = null;
		playerCountViewController.gameObject.SetActive(false);
	}
}