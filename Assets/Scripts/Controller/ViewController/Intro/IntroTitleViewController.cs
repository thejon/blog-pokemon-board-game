﻿using UnityEngine;
using System;

public class IntroTitleViewController : BaseViewController {

	public Action didFinish;

	public void PlayButtonPressed () {
		if (didFinish != null)
			didFinish();
	}
}
