﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GetSetViewController : BaseViewController {

	public enum Exits {
		Roll,
		Team
	}

	public Action<Exits> didFinish;

	[SerializeField] Text playerNameLabel;
	[SerializeField] Sprite emptyBadge;
	[SerializeField] Image[] badges;

	void OnEnable () {
		var player = game.CurrentPlayer;
		playerNameLabel.text = player.nickName;
		for (int i = 0; i < badges.Length; ++i) {
			if (player.badges.Count > i)
				badges[i].sprite = GymSystem.GetBadge(player.badges[i]);
			else
				badges[i].sprite = emptyBadge;
		}
	}

	public void OnRollPressed () {
		if (didFinish != null)
			didFinish (Exits.Roll);
	}

	public void OnTeamPressed () {
		if (didFinish != null)
			didFinish (Exits.Team);
	}
}