﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class RollViewController : BaseViewController {

	public Action didComplete;
	[SerializeField] Text rollLabel;

	protected override void DidShow (Action complete) {
		base.DidShow (complete);
		StartCoroutine(RollSequence());
	}

	IEnumerator RollSequence () {
		int roll = 0;
		for (int i = 0; i < 10; ++i) {
			roll = UnityEngine.Random.Range(2, 13);
			rollLabel.text = roll.ToString();
			yield return new WaitForSeconds(1.0f / 30.0f);
		}
		yield return new WaitForSeconds(1);

		var currentPlayer = game.CurrentPlayer;
		currentPlayer.destinationTileIndex = (currentPlayer.tileIndex + roll) % board.tiles.Count;
		currentPlayer.candies += roll;

		if (didComplete != null)
			didComplete();
	}
}