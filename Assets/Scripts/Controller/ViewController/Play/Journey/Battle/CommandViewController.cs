﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;
using ECS;

public class CommandViewController : BaseViewController {

	public enum Exits {
		FastMove,
		ChargeMove,
		Capture,
		Retreat
	}

	public Action<Exits> didFinish;

	public Button chargeButton;
	public Button captureButton;
	public Button retreatButton;

	void OnEnable () {
		chargeButton.interactable = CanSelectChargeMove();
	}

	public void SetupGymBattle () {
		captureButton.gameObject.SetActive (false);
		retreatButton.gameObject.SetActive (true);
	}

	public void SetupEncounterBattle () {
		captureButton.gameObject.SetActive (true);
		retreatButton.gameObject.SetActive (false);
	}

	public void OnFastMovePressed () {
		if (didFinish != null)
			didFinish(Exits.FastMove);
	}

	public void OnChargeMovePressed () {
		if (didFinish != null)
			didFinish(Exits.ChargeMove);
	}

	public void OnCapturePressed () {
		if (didFinish != null)
			didFinish(Exits.Capture);
	}

	public void OnRetreatPressed () {
		if (didFinish != null)
			didFinish(Exits.Retreat);
	}

	bool CanSelectChargeMove () {
		var pokemon = battle.combatants[0].CurrentPokemon;
		var cost = Mathf.Abs(pokemon.ChargeMove.energy);
		return pokemon.energy >= cost;
	}
}