﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class VictoryViewController : BaseViewController {

	public Action didFinish;

	[SerializeField] Image reward;
	[SerializeField] Text infoLabel;

	public override void Show (Action didShow) {
		var gym = GymSystem.GetCurrentGym (game, board);
		infoLabel.text = string.Format ("You've earned the\n{0} badge.", gym.type);
		reward.sprite = GymSystem.GetBadge (gym);
		GymSystem.AwardBadge (currentPlayer, gym);

		reward.transform.localEulerAngles = Vector3.zero;
		reward.transform.RotateToLocal (new Vector3 (0, 360, 0), 1f, EasingEquations.EaseInExpo);
		reward.transform.localScale = Vector3.zero;

		base.Show (didShow);
	}

	protected override void DidShow (Action complete) {
		base.DidShow (complete);
		Tweener tweener = reward.transform.ScaleTo (Vector3.one);
		tweener.duration = 1f;
		tweener.equation = EasingEquations.EaseOutBounce;
	}

	public void OnContinueButtonPressed () {
		if (didFinish != null)
			didFinish ();
	}
}