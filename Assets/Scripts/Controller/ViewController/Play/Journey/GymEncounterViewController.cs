﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class GymEncounterViewController : BaseViewController {

	public enum Exits {
		Challenge,
		Ignore
	}
	public Action<Exits> didFinish;

	[SerializeField] List<Image> avatars;
	[SerializeField] List<Text> cpLabels;
	[SerializeField] Text titleLabel;

	public void Show (Gym gym, Action didShow = null) {
		titleLabel.text = string.Format("{0} Gym", gym.type);
		for (int i = 0; i < avatars.Count; ++i) {
			avatars[i].sprite = gym.pokemon[i].GetAvatar();
			cpLabels[i].text = string.Format("CP: {0}",gym.pokemon[i].CP);
		}
		Show (didShow);
	}

	public void ChallengeButtonPressed () {
		if (didFinish != null)
			didFinish(Exits.Challenge);
	}

	public void IgnoreButtonPressed () {
		if (didFinish != null)
			didFinish(Exits.Ignore);
	}
}
