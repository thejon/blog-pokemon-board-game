﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicController : MonoBehaviour {
	[SerializeField] AudioSource currentMusic;
	[SerializeField] AudioSource introMusic;
	[SerializeField] List<AudioSource> journeyMusic;
	[SerializeField] List<AudioSource> battleMusic;
	[SerializeField] List<AudioSource> victoryMusic;
	[SerializeField] AudioSource endMusic;

	public void PlayIntro () {
		SetSource(introMusic);
	}

	public void PlayJourney (int level) {
		level = Mathf.Clamp(level, 0, journeyMusic.Count - 1);
		SetSource(journeyMusic[level]);
	}

	public void PlayBattle (int level) {
		level = Mathf.Clamp(level, 0, battleMusic.Count - 1);
		SetSource(battleMusic[level], true);
	}

	public void PlayVictory (int level) {
		level = Mathf.Clamp (level, 0, victoryMusic.Count - 1);
		SetSource (victoryMusic [level], true);
	}

	public void PlayGameOver () {
		SetSource(endMusic, true);
	}

	void SetSource (AudioSource source, bool forceReset = false) {
		if (source == currentMusic)
			return;

		if (currentMusic != null)
			currentMusic.Pause();
		
		currentMusic = source;
		if (forceReset)
			currentMusic.time = 0;
		currentMusic.Play();
	}
}
