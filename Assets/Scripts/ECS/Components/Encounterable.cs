﻿using SQLite4Unity3d;

namespace ECS {
	public class Encounterable {
		public const int ComponentID = 3;

		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public double rate { get; set; }
	}

	public static class EncounterableExtensions {
		public static Encounterable GetEncounterable (this Entity entity) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.entity_id == entity.id && x.component_id == Encounterable.ComponentID)
				.FirstOrDefault();
			if (entityComponent == null) return null;
			var component = connection.Table<Encounterable>()
				.Where(x => x.id == entityComponent.component_data_id)
				.FirstOrDefault();
			return component;
		}

		public static Entity GetEntity (this Encounterable item) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.component_id == Encounterable.ComponentID && x.component_data_id == item.id)
				.FirstOrDefault();
			var entity = connection.Table<Entity>()
				.Where(x => x.id == entityComponent.entity_id)
				.FirstOrDefault();
			return entity;
		}
	}
}