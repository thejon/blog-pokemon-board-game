﻿using SQLite4Unity3d;

namespace ECS {
	public class TypeMultiplier {
		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public int attack_type_id { get; set; }
		public int defend_type_id { get; set; }
		public double value { get; set; }
	}
}