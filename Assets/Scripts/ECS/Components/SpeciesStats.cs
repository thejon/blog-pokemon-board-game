﻿using SQLite4Unity3d;

namespace ECS {
	public class SpeciesStats {
		public const int ComponentID = 1;

		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string name { get; set; }
		public int typeA { get; set; }
		public int typeB { get; set; }
		public int maxCP { get; set; }
		public int attack { get; set; }
		public int defense { get; set; }
		public int stamina { get; set; }
	}

	public static class SpeciesStatsExtensions {
		public static SpeciesStats GetSpeciesStats (this Entity entity) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.entity_id == entity.id && x.component_id == SpeciesStats.ComponentID)
				.FirstOrDefault();
			if (entityComponent == null) return null;
			var component = connection.Table<SpeciesStats>()
				.Where(x => x.id == entityComponent.component_data_id)
				.FirstOrDefault();
			return component;
		}

		public static Entity GetEntity (this SpeciesStats item) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.component_id == SpeciesStats.ComponentID && x.component_data_id == item.id)
				.FirstOrDefault();
			var entity = connection.Table<Entity>()
				.Where(x => x.id == entityComponent.entity_id)
				.FirstOrDefault();
			return entity;
		}
	}
}