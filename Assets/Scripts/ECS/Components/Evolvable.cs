﻿using SQLite4Unity3d;

namespace ECS {
	public class Evolvable {
		public const int ComponentID = 2;

		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public int entity_id { get; set; }
		public int cost { get; set; }
	}

	public static class EvolvableExtensions {
		public static Evolvable GetEvolvable (this Entity entity) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.entity_id == entity.id && x.component_id == Evolvable.ComponentID)
				.FirstOrDefault();
			if (entityComponent == null) return null;
			var component = connection.Table<Evolvable>()
				.Where(x => x.id == entityComponent.component_data_id)
				.FirstOrDefault();
			return component;
		}

		public static Entity GetEntity (this Evolvable item) {
			var connection = DataController.instance.pokemonDatabase.connection;
			var entityComponent = connection.Table<EntityComponent>()
				.Where(x => x.component_id == Evolvable.ComponentID && x.component_data_id == item.id)
				.FirstOrDefault();
			var entity = connection.Table<Entity>()
				.Where(x => x.id == entityComponent.entity_id)
				.FirstOrDefault();
			return entity;
		}
	}
}