﻿using SQLite4Unity3d;

namespace ECS {
	public class Component {
		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string name { get; set; }
		public string description { get; set; }
	}
}