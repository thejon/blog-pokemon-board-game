﻿using SQLite4Unity3d;

namespace ECS {
	public class Entity {
		[PrimaryKey, AutoIncrement]
		public int id { get; set; }
		public string label { get; set; }
	}
}