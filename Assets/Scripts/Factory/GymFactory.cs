﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ECS;

public static class GymFactory {
	public static List<Gym> CreateGyms (int[] levels) {
		int count = levels.Length;
		var connection = DataController.instance.pokemonDatabase.connection;
		var types = new List<ECS.Type>(connection.Table<ECS.Type>());
		int poolSize = 3;

		var retValue = new List<Gym>(count);
		while (retValue.Count < count && types.Count > 0) {
			int random = UnityEngine.Random.Range(0, types.Count);
			var type = types[random];
			types.RemoveAt(random);

			var stats = type.StatsWithType();
			if (stats.Count < poolSize)
				continue;

			var gym = CreateGym(type, stats, poolSize);
			retValue.Add(gym);
			poolSize++;
		}

		retValue.Sort(SortAscendingByMaxCP);

		for (int i = 0; i < retValue.Count; ++i) {
			foreach (Pokemon pokemon in retValue[i].pokemon) {
				pokemon.SetLevel(levels[i]);
			}
		}
		return retValue;
	}

	static Gym CreateGym (Type type, List<SpeciesStats> stats, int poolSize) {
		List<SpeciesStats> candidates = new List<SpeciesStats>(poolSize);
		for (int i = 0; i < poolSize; ++i) {
			int random = UnityEngine.Random.Range(0, stats.Count);
			candidates.Add(stats[random]);
			stats.RemoveAt(random);
		}
		candidates.Sort(SortDescendingByMaxCP);

		Gym gym = new Gym ();
		gym.type = type.name;
		gym.pokemon.Add( PokemonFactory.Create(candidates[1].GetEntity()) );
		gym.pokemon.Add( PokemonFactory.Create(candidates[0].GetEntity()) );
		return gym;
	}

	static int SortDescendingByMaxCP (SpeciesStats x, SpeciesStats y) {
		return y.maxCP.CompareTo(x.maxCP);
	}

	static int SortAscendingByMaxCP (Gym x, Gym y) {
		int xMax = Mathf.Max(x.pokemon[0].Stats.maxCP, x.pokemon[1].Stats.maxCP);
		int yMax = Mathf.Max(y.pokemon[0].Stats.maxCP, y.pokemon[1].Stats.maxCP);
		return xMax.CompareTo(yMax);
	}
}