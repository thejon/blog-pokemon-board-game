﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameFactory {
	public static Game Create (int playerCount) {
		Game game = new Game ();
		game.players = new List<Player> (playerCount);
		for (int i = 0; i < playerCount; ++i) {
			var player = PlayerFactory.Create ();
			game.players.Add (player);
		}
		var levels = new int[4] { 5, 10, 15, 20 };
		game.gyms = GymFactory.CreateGyms (levels);
		return game;
	}

	public static Game Create (string json) {
		Game game = JsonUtility.FromJson<Game> (json);
		foreach (Player player in game.players) {
			foreach (Pokemon pokemon in player.pokemon) {
				pokemon.Restore ();
			}
		}
		foreach (Gym gym in game.gyms) {
			foreach (Pokemon pokemon in gym.pokemon) {
				pokemon.Restore ();
			}
		}
		return game;
	}
}