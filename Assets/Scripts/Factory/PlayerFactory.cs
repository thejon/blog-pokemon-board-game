﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PlayerFactory {
	public static Player Create () {
		var player = new Player ();
		player.pokeballs = 5;
		player.revives = 1;
		player.potions = 1;
		return player;
	}
}